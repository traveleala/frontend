const Home = () =>
    import ("./home/Home");
const Activity = () =>
    import ("./activity/Activity");
const Activities = () =>
    import ("./activities/Activities");
const Terms = () =>
    import ("./terms/Terms");
const Faq = () =>
    import ("./faq/Faq");
const Profile = () =>
    import ("./profile/Profile");
const NotFound = () =>
    import ("../common/notfound/NotFound");
const Users = () =>
    import ("./users/Users");

const publicRoutes = [
    { path: "/api" },
    {
        path: "/",
        component: Home,
        name: "Home",
    },
    {
        path: "/login/:social?",
        component: Users,
        name: "Login",
    },
    {
        path: "/register",
        component: Users,
        name: "Register",
    },
    {
        path: "/send-verification",
        component: Users,
        name: "Verification",
    },
    {
        path: "/reset-password",
        component: Users,
        name: "ResetPassword",
    },
    {
        path: "/activities/search/:location?",
        component: Activities,
        name: "Activities",
    },
    {
        path: "/activities/:id",
        name: "Activity",
        component: Activity,
    },
    {
        path: "/faq",
        component: Faq,
    },
    {
        path: "/terms",
        component: Terms,
    },
    {
        path: "/users/:username",
        component: Profile,
        name: "Profile",
    },
    {
        path: "*",
        component: NotFound,
        name: "NotFound",
    },
];

export default publicRoutes;