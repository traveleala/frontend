export default {
    downRecents: {
        orderBy: 'created_at',
        order: 'asc'
    },
    upRecents: {
        orderBy: 'created_at',
        order: 'desc'
    },
    upQualifications: {
        orderBy: 'qualification',
        order: 'desc'
    },
    downQualifications: {
        orderBy: 'qualification',
        order: 'asc'
    },
    upPrice: {
        orderBy: 'price',
        order: 'desc'
    },
    downPrice: {
        orderBy: 'price',
        order: 'asc'
    },
    nearDistance: {
        orderBy: 'distance',
        order: 'asc'
    },
    farDistance: {
        orderBy: 'distance',
        order: 'desc'
    }

};