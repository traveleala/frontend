import queryParam from "~c/helpers/queryParam";
import handleSort from "./handleSort";

export default (filtersSearch) => {
    let url = filtersSearch.location;
    let api =
        filtersSearch.location != "" ? `?location=${filtersSearch.location}` : "";

    Object.entries(filtersSearch).forEach(([key, value]) => {
        if (value != "") {
            if (key == "sort") {
                url = handleSort(url, value);
                api = handleSort(api, value);
            } else {
                if (key != "location") {
                    url = queryParam(url, key, value);
                }

                api = queryParam(api, key, value);
            }
        }
    });

    return {
        url: url,
        api: api,
    };
};