import sorts from "../constants/sorts";

export default (orderBy, order) => {

    if (!orderBy || !order) {
        return '';
    }

    let key = '';

    for (const sort in sorts) {
        const value = sorts[sort];

        if (value.orderBy == orderBy && value.order == order) {
            key = sort;
            break;
        }
    }

    return key;
}