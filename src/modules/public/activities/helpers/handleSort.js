import queryParam from "~c/helpers/queryParam";
import dictionary from "../constants/sorts";

export default (url, sort) => {

    for (const item in dictionary[sort]) {
        const value = dictionary[sort][item];
        url = queryParam(url, item, value);
    }

    return url;
}