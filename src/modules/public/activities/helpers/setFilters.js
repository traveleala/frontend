const queryString = require("query-string");
import setOrder from "./setOrder";

export default (url) => {
    let {
        landscape,
        distance,
        maxPrice,
        minPrice,
        title,
        orderBy,
        order,
        page,
        coordinates,
        types,
        difficulties
    } = queryString.parse(url.search);

    const sort = setOrder(orderBy, order);

    if (coordinates) {
        coordinates = coordinates.split(",");
        coordinates = [parseFloat(coordinates[0]), parseFloat(coordinates[1])];
    }

    if (types) {
        types = types.split(",");
    }

    if (difficulties) {
        difficulties = difficulties.split(",");
    }

    return {
        landscape,
        distance,
        maxPrice,
        minPrice,
        title,
        sort,
        page,
        coordinates,
        types,
        difficulties
    };
};