const Dashboard = () =>
    import ("./dashboard/Dashboard");

import route from "~c/helpers/route";
import store from "~c/store";

const user = store.getters.user;

const userRoutes = [{
    path: "/users/me/:section/:subSection?/:thirdSection?",
    name: "Dashboard",
    component: Dashboard,
    beforeEnter: (to, from, next) => {
        if (!user) {
            next(route("public.users.login"));
        } else {
            next();
        }
    },
}, ];

export default userRoutes;