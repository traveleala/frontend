export default (months, publishType) => {

    let partial = 0;
    let discount = 0;
    let total = 0;

    if (
        publishType == "" ||
        publishType.price != 0
    ) {
        partial = months * publishType.price;
        discount = (partial * publishType.discount + 1) * -1;
        total = partial + discount;
    } else {
        total = "Gratis";
    }

    return {
        partial: partial,
        discount: discount,
        total: total
    }
}