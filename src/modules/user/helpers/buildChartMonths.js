export default (valuesPerMonth) => {
    let values = {};

    valuesPerMonth.map(valuePerMonth => {
        values[valuePerMonth.month] = valuePerMonth.total;
    });

    let arrayMonths = [];

    for (let index = 1; index <= 12; index++) {
        const valuePerMonth = parseFloat(values[index]) || 0;
        arrayMonths.push(valuePerMonth);
    }

    return arrayMonths;
}