export default {
    url: "https://traveleala.org/",
    api: {
        v1: "https://traveleala.org/api/v1/",
    },
    appName: "Traveleala",
    googleAnalytics: "UA-151916640-1",
    captchaKey: "6LdOKvsUAAAAAE9UKWrKk-6ZydTOYrCq3A6HB7D0",
    sentry: "",
    socialMediaUrls: {
        facebook: "https://facebook.com/",
        instagram: "https://instagram.com/",
    },
    map: {
        defaultLongitude: "-64.489405",
        defaultLatitude: "-34.692538",
        zoom: 10,
    },
};