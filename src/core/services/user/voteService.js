import api from "../../helpers/api";
import http from "../baseService";

export default {
    vote: (qualificationId) => {
        const url = api("v1.qualifications.votes.update", { id: qualificationId });
        return http.put(url);
    },
};