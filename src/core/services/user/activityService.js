import api from "../../helpers/api";
import http from "../baseService";

export default {
    recents: () => {
        const url = api("v1.activities.recents");
        return http.get(url);
    },
    top: () => {
        const url = api("v1.activities.top");
        return http.get(url);
    },
    search: (queryParams = "") => {
        const baseUrl = api("v1.activities.search");
        const url = `${baseUrl}${queryParams}`;
        return http.get(url);
    },
    show: (id) => {
        const url = api("v1.activities.show", { id: id });
        return http.get(url);
    },
    relateds: (id) => {
        const url = api("v1.activities.relateds", { id: id });
        return http.get(url);
    },
    byUsername: (username, page) => {
        const url = api("v1.activities.byUsername", {
            username: username,
            page: page,
        });

        return http.get(url);
    },
    users: (id) => {
        const url = api("v1.activities.users", { id: id });
        return http.get(url);
    },
    total: () => {
        const url = api("v1.activities.total");
        return http.get(url);
    },
    perMonth: (year) => {
        const url = api("v1.activities.perMonth", { year: year });
        return http.get(url);
    },
    step: (step, formData) => {
        const url = api("v1.activities.step", { step: step });
        return http.post(url, formData);
    },
    publishes: () => {
        const url = api("v1.activities.publishes");
        return http.get(url);
    },
    store: (data) => {
        const url = api("v1.activities.store");
        return http.post(url, data);
    },
    update: (id, data) => {
        const url = api("v1.activities.update", { id: id });
        data.append('_method', 'put');
        return http.post(url, data);
    },
    myActivities: (page) => {
        const url = api("v1.activities.myActivities", { page: page });
        return http.get(url);
    },
    getTotalMyActivities: () => {
        const url = api("v1.activities.myActivitiesTotal");
        return http.get(url);
    },
    delete: (id) => {
        const url = api("v1.activities.delete", { id: id });
        return http.delete(url);
    },
    types: () => {
        const url = api("v1.activities.types");
        return http.get(url);
    },
    difficulties: () => {
        const url = api("v1.activities.difficulties");
        return http.get(url);
    },
    statistics: (id) => {
        const url = api("v1.activities.statistics", { id: id });
        return http.get(url);
    },
    sharedIn: (id, sharedIn) => {
        const url = api("v1.activities.sharedIn", { id: id, sharedIn: sharedIn });
        return http.get(url);
    },
    showMyActivity: (id) => {
        const url = api("v1.activities.showMyActivity", { id: id });
        return http.get(url);
    }
};