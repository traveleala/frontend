import api from "../../helpers/api";
import http from "../baseService";

export default {
    get: (id, page) => {
        const url = api("v1.qualifications.get", { id: id, page: page });
        return http.get(url);
    },
    getTotal: (id) => {
        const url = api("v1.qualifications.getTotal", { id: id });
        return http.get(url);
    },
    show: (id) => {
        const url = api("v1.qualifications.show", { id: id });
        return http.get(url);
    },
    store: (id, formData) => {
        const url = api("v1.qualifications.store", { id: id });
        return http.post(url, formData);
    },
    total: () => {
        const url = api("v1.qualifications.total");
        return http.get(url);
    },
    perMonth: (year) => {
        const url = api("v1.qualifications.perMonth", { year: year });
        return http.get(url);
    },
    statisticsTotal: (id) => {
        const url = api("v1.qualifications.statistics.total", { id: id });
        return http.get(url);
    },
    statisticsPerMonth: (id, year) => {
        const url = api("v1.qualifications.statistics.perMonth", {
            id: id,
            year: year,
        });
        return http.get(url);
    },
    getDifficulties: (id) => {
        const url = api("v1.qualifications.difficulties", { id: id });
        return http.get(url);
    },
    getStars: (id) => {
        const url = api("v1.qualifications.stars", { id: id });
        return http.get(url);
    },
};