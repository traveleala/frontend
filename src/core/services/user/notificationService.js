import api from "../../helpers/api";
import http from "../baseService";

export default {
    news: () => {
        const url = api("v1.notifications.news");
        return http.get(url);
    },
    get: (page) => {
        const url = api("v1.notifications.get", { page: page });
        return http.get(url);
    },
};