import api from "../../helpers/api";
import http from "../baseService";

export default {
    search: (search) => {
        const url = api("v1.locations.search", { search: search });
        return http.get(url);
    },
    top: () => {
        const url = api("v1.locations.top");
        return http.get(url);
    },
};