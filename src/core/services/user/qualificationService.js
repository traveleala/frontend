import api from "../../helpers/api";
import http from "../baseService";

export default {
    total: () => {
        const url = api("v1.qualification.total");
        return http.get(url);
    },
    perMonth: (year) => {
        const url = api("v1.qualification.perMonth", { year: year });
        return http.get(url);
    },
    statisticsTotal: (id) => {
        const url = api("v1.qualification.statistics.total", { id: id });
        return http.get(url);
    },
    statisticsPerMonth: (id, year) => {
        const url = api("v1.qualification.statistics.perMonth", {
            id: id,
            year: year,
        });

        return http.get(url);
    },
};