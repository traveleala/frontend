import api from "../../helpers/api";
import http from "../baseService";

export default {
    upload: (formData) => {
        const url = api("v1.activities.images.upload");
        return http.post(url, formData);
    },
    delete: (formData) => {
        formData.append("_method", "delete");
        const url = api("v1.activities.images.delete");
        return http.post(url, formData);
    },
};