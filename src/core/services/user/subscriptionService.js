import api from "../../helpers/api";
import http from "../baseService";

export default {
    update: (id, formData) => {
        formData.append("_method", "put");
        const url = api("v1.subscriptions.update", { id: id });
        return http.post(url, formData);
    },
    pay: (id) => {
        const url = api("v1.subscriptions.pay", { id: id });
        return http.post(url);
    },
};