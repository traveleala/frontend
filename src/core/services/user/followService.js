import api from "../../helpers/api";
import http from "../baseService";

export default {
    show: (username) => {
        const url = api("v1.follow.show", { username: username });
        return http.get(url);
    },
    update: (username) => {
        const url = api("v1.follow.update", { username: username });
        return http.put(url);
    },
    followings: (page) => {
        const url = api("v1.follow.followings", { page: page });
        return http.get(url);
    },
    followers: (page) => {
        const url = api("v1.follow.followers", { page: page });
        return http.get(url);
    },
    getTotalFollowers: () => {
        const url = api("v1.follow.totalFollowers");
        return http.get(url);
    },
    getTotalFollowings: () => {
        const url = api("v1.follow.totalFollowings");
        return http.get(url);
    }
};