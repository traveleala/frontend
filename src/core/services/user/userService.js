import api from "../../helpers/api";
import http from "../baseService";

export default {
    login: (formData) => {
        const url = api("v1.users.login");
        return http.post(url, formData);
    },
    me: () => {
        const url = api("v1.users.me");
        return http.get(url);
    },
    register: (formData) => {
        const url = api("v1.users.register");
        return http.post(url, formData);
    },
    resend: (formData) => {
        const url = api("v1.users.resend");
        return http.post(url, formData);
    },
    reset: (formData) => {
        const url = api("v1.users.reset");
        return http.post(url, formData);
    },
    resetPassword: (code, formData) => {
        formData.append("_method", "put");
        const url = api("v1.users.resetPassword", { code: code });
        return http.post(url, formData);
    },
    logout: () => {
        const url = api("v1.users.logout");
        return http.delete(url);
    },
    verification: (code) => {
        const url = api("v1.users.verification", { code: code });
        return http.put(url);
    },
    deleteAccount: (formData) => {
        formData.append("_method", "delete");
        const url = api("v1.users.me");
        return http.post(url, formData);
    },
    password: (formData) => {
        formData.append("_method", "put");
        const url = api("v1.users.password");
        return http.post(url, formData);
    },
    socialAuth: (socialNetwork) => {
        const url = api("v1.users.socialAuth", { socialNetwork: socialNetwork });
        return http.post(url);
    },
    loginWith: (socialNetwork, code) => {
        const url = api("v1.users.loginWith", {
            socialNetwork: socialNetwork,
            code: code,
        });
        return http.post(url);
    },
};