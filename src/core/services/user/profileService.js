import api from "../../helpers/api";
import http from "../baseService";

export default {
    show: (username) => {
        const url = api("v1.profile.show", { username: username });
        return http.get(url);
    },
    me: () => {
        const url = api("v1.profile.me");
        return http.get(url);
    },
    update: (formData) => {
        formData.append("_method", "put");
        const url = api("v1.profile.update");
        return http.post(url, formData);
    },
};