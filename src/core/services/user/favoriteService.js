import api from "../../helpers/api";
import http from "../baseService";

export default {
    show: (id) => {
        const baseUrl = api("v1.favorites.show");
        const url = `${baseUrl}?activity_id=${id}`;
        return http.get(url);
    },
    all: (page) => {
        const url = api("v1.favorites.all", { page: page });
        return http.get(url);
    },
    getTotal: () => {
        const url = api("v1.favorites.total");
        return http.get(url);
    },
    update: (id) => {
        const url = api("v1.favorites.update", { id: id });
        return http.put(url);
    },
};