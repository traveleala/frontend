import api from "../../helpers/api";
import http from "../baseService";

export default {
    types: () => {
        const url = api("v1.reports.types");
        return http.get(url);
    },
    store: (id, typeReport, formData) => {
        const url = api(`v1.reports.store.${typeReport}`, { id: id });

        return http.post(url, formData);
    },
};