import api from "../../helpers/api";
import http from "../baseService";

export default {
    perMonth: (year) => {
        const url = api("v1.visits.perMonth", { year: year });
        return http.get(url);
    },
    total: () => {
        const url = api("v1.visits.total");
        return http.get(url);
    },
    statisticsTotal: (id) => {
        const url = api("v1.visits.statistics.total", { id: id });
        return http.get(url);
    },
    statisticsPerMonth: (id, year) => {
        const url = api("v1.visits.statistics.perMonth", { id: id, year: year });
        return http.get(url);
    },
};