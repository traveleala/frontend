import axios from "axios";
import storage from "../helpers/storage";
import store from "../store";

let headers = {
    "Content-Type": "multipart/form-data",
    Accept: "application/json",
};

let axiosInstance = axios.create({
    headers: headers,
});

axiosInstance.interceptors.request.use((config) => {
    const token = storage.get("token");
    if (token && token != "") {
        config.headers["Authorization"] = "Bearer " + token;
    }

    return config;
});

axiosInstance.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        const status = error.response.data.status;

        if (status == 401) {
            store.commit("removeUser");
            return false;
        }

        return Promise.reject(error);
    }
);

export default axiosInstance;