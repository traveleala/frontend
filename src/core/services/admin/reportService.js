import api from "../../helpers/api";
import http from "../baseService";

export default {
    reports: (page) => {
        const url = api("v1.admin.reports.get", { page: page });
        return http.get(url);
    },
    actions: (entity) => {
        const url = api("v1.admin.reports.actions", { entity: entity });
        return http.get(url);
    },
    process: (id, formData) => {
        formData.append("_method", "put");
        const url = api("v1.admin.reports.process", { id: id });
        return http.post(url, formData);
    },
};