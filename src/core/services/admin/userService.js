import api from "../../helpers/api";
import http from "../baseService";

export default {
    users: (page) => {
        const url = api("v1.admin.users.get", { page: page });
        return http.get(url);
    },
    createUser: (formData) => {
        const url = api("v1.admin.users.create");
        return http.post(url, formData);
    },
    deleteUser: (formData) => {
        formData.append("_method", "delete");
        const url = api("v1.admin.users.delete");
        return http.post(url, formData);
    },
    roles: () => {
        const url = api("v1.admin.users.roles");
        return http.get(url);
    },
};