import Vue from "vue";
import route from "../helpers/route";
import api from "../helpers/api";
import haversine from "../helpers/haversine";
import date from "../helpers/date";

Vue.mixin({
    methods: {
        route: route,
        api: api,
        haversine: haversine,
        date: date,
    },
});