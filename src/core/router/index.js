import Vue from "vue";
import Router from "vue-router";

// Routes
import Public from "~m/public/public.router";
import User from "~m/user/user.router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [...Public, ...User],
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },
});