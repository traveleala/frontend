import Vuex from "vuex";
import Vue from "vue";
import storage from "~c/helpers/storage";
import route from "~c/helpers/route";
import userService from "~c/services/user/userService";
import notification from "../helpers/notification";
import roles from "./constants/roles";
//import router from "../router";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: "",
    },
    getters: {
        user: () => {
            return storage.get("user");
        },
        isLoggedIn: () => {
            return storage.get("user").role;
        },
        isNotUser: () => {
            return (
                storage.get("user").role == roles.admin ||
                storage.get("user").role == roles.publisher
            );
        },
        isAdmin: () => {
            return storage.get("user").role == roles.admin;
        },
        isMyProperty: () => {
            return (username) =>
                storage.get("user") && storage.get("user").profile.username == username;
        },
        isSocial: () => {
            return storage.get("user") && storage.get("user").isSocial;
        },
    },
    mutations: {
        setUser: (state, token) => {
            storage.set("token", token);

            userService
                .me()
                .then((response) => {
                    notification(
                        "success",
                        "Ingresaste correctamente, seras redirigido en unos instantes"
                    );
                    storage.set("user", response.data.user);
                    window.location = route("public.home");
                })
                .catch(() => {
                    storage.remove("token");
                    notification(
                        "error",
                        "Tenemos un problema con el ingreso a la app, intentalo nuevamente"
                    );
                });
        },
        updateUser: (state) => {
            userService
                .me()
                .then((response) => {
                    storage.set("user", response.data.user);
                    state.user = storage.get("user");
                })
                .catch(() => {});
        },
        logout: () => {
            userService
                .logout()
                .then(() => {})
                .catch(() => {});
        },
        removeUser: (state) => {
            storage.remove("user");
            storage.remove("token");
            state.user = "";
            window.location = route("public.home");


        },
    },
});