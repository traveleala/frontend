// Components
import "pretty-checkbox/src/pretty-checkbox.scss";
import "vue-multi-select/dist/lib/vue-multi-select.css";
import "vuelayers/lib/style.css";
import "izitoast/dist/css/iziToast.min.css";
import "line-icons/font-css/LineIcons.css";

// App styles
import "./sass/app.scss";

// Fonts
require("typeface-varela");