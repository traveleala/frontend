import iziToast from "izitoast/dist/js/iziToast.min.js";

export default (method, message) => {
    const configDefault = {
        position: "bottomLeft",
        messageSize: "18",
        maxWidth: "300px",
        message: message,
        backgroundColor: "#fff",
        theme: "traveleala",
        timeout: 5000,
        icon: null,
        progressBar: false,
        closeOnClick: true,
        close: false,
        animateInside: false,
    };

    switch (method) {
        case "info":
            iziToast.info(configDefault);
            break;
        case "error":
            iziToast.error(configDefault);
            break;
        case "success":
            iziToast.success(configDefault);
            break;
        case "warning":
            iziToast.warning(configDefault);
            break;
    }
};