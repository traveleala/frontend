const date = require("date-and-time");
require("date-and-time/locale/es");

date.locale("es");

export default (dateTime) => {
    const now = new Date(dateTime);
    const pattern = date.compile("MMM de YYYY");

    return date.format(now, pattern);
};