import FormData from "form-data";

export default (object) => {
    let formData = new FormData;

    for (const key in object) {
        const value = object[key];

        formData.append(key, value);
    }

    return formData;
}