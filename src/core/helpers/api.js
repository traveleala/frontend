import endpoints from "./constants/endpoints";

const Api = (name, values = {}) => {
    const partsRoute = name.split(".");
    let routeUrl = endpoints;

    partsRoute.forEach(index => {
        routeUrl = routeUrl[index];
    });

    Object.entries(values).forEach(item => {
        let key = item[0];
        let value = item[1];

        window[key] = value;
    });

    routeUrl = eval(`\`${routeUrl}\``);

    return routeUrl;
};

export default Api;