const haversine = require("haversine");

export default (latitude, longitude) => {
    const start = {
        latitude: localStorage.getItem("latitude"),
        longitude: localStorage.getItem("longitude"),
    };

    const end = {
        latitude: latitude,
        longitude: longitude,
    };

    const kms = haversine(start, end)
        .toFixed(1)
        .replace(".", ",");

    return `${kms} kms`;
};