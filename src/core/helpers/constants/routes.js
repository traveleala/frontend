export default {
    public: {
        home: "/",
        activities: {
            search: "/activities/search",
            view: "/activities/${id}",
        },
        profile: "/users/${username}",
        terms: "/terms",
        faq: "/faq",
        users: {
            login: "/login",
            register: "/register",
            verification: "/send-verification",
            resetPassword: "/reset-password",
        },
    },
    user: {
        dashboard: "/users/me/dashboard",
        profile: "/users/me/profile",
        notifications: "/users/me/notifications",
        password: "/users/me/password",
        favorites: "/users/me/favorites",
        activities: {
            home: "/users/me/activities",
            new: "/users/me/activities/create",
            statistics: "/users/me/activities/${id}/statistics",
            update: "/users/me/activities/${id}/update"
        },
        users: {
            followers: "/users/me/followers",
            followings: "/users/me/followings",
        },
        admin: {
            users: "/users/me/users",
            reports: "/users/me/reports",
        },
    },
};