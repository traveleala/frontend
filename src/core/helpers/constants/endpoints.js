import config from "../../config";

const v1 = config.api.v1;

export default {
    v1: {
        activities: {
            top: v1 + "activities/top",
            recents: v1 + "activities/recents",
            search: v1 + "activities",
            show: v1 + "activities/${id}",
            relateds: v1 + "activities/${id}/relateds",
            byUsername: v1 + "users/${username}/activities?page=${page}",
            users: v1 + "activities/${id}/users",
            total: v1 + "users/me/activities/statistics/total",
            perMonth: v1 + "users/me/activities/statistics/${year}/per-month",
            step: v1 + "users/me/activities/steps/${step}",
            publishes: v1 + "activities/publishes",
            images: {
                upload: v1 + "users/me/activities/images",
                delete: v1 + "users/me/activities/images",
            },
            store: v1 + "users/me/activities",
            update: v1 + "users/me/activities/${id}",
            myActivities: v1 + "users/me/activities?page=${page}",
            myActivitiesTotal: v1 + "users/me/activities?total",
            delete: v1 + "users/me/activities/${id}",
            types: v1 + "activities/types",
            difficulties: v1 + "activities/difficulties",
            statistics: v1 + "users/me/activities/${id}/statistics",
            sharedIn: v1 + "users/me/activities/${id}/statistics/shares/${sharedIn}",
            showMyActivity: v1 + "users/me/activities/${id}"
        },
        favorites: {
            show: v1 + "users/me/favorites/activities",
            update: v1 + "activities/${id}/favorites",
            all: v1 + "users/me/favorites/activities?page=${page}",
            total: v1 + "users/me/favorites/activities?total"
        },
        follow: {
            show: v1 + "users/${username}/follow",
            update: v1 + "users/${username}/follow",
            followers: v1 + "users/me/followers?page=${page}",
            followings: v1 + "users/me/followings?page=${page}",
            totalFollowers: v1 + "users/me/followers?total",
            totalFollowings: v1 + "users/me/followings?total",
        },
        locations: {
            search: v1 + "locations?name=${search}",
            top: v1 + "locations/top",
        },
        notifications: {
            get: v1 + "users/me/notifications?page=${page}",
            news: v1 + "users/me/notifications/news",
        },
        profile: {
            show: v1 + "users/${username}",
            me: v1 + "users/me/profile",
            update: v1 + "users/me/profile",
        },
        provinces: {
            top: v1 + "provinces/top",
        },
        qualification: {
            total: v1 + "users/me/qualification/statistics/total",
            perMonth: v1 + "users/me/qualification/statistics/${year}/per-month",
            statistics: {
                total: v1 + "users/me/activities/${id}/statistics/qualification/total",
                perMonth: v1 +
                    "users/me/activities/${id}/statistics/qualification/${year}/per-month",
            },
        },
        qualifications: {
            get: v1 + "activities/${id}/qualifications?page=${page}",
            getTotal: v1 + "activities/${id}/qualifications?total",
            show: v1 + "activities/${id}/qualifications/${id}",
            store: v1 + "activities/${id}/qualifications",
            votes: {
                update: v1 + "qualifications/${id}/votes",
            },
            total: v1 + "users/me/qualifications/statistics/total",
            perMonth: v1 + "users/me/qualifications/statistics/${year}/per-month",
            statistics: {
                total: v1 + "users/me/activities/${id}/statistics/qualifications/total",
                perMonth: v1 +
                    "users/me/activities/${id}/statistics/qualifications/${year}/per-month",
            },
            difficulties: v1 + "activities/${id}/qualifications/difficulties",
            stars: v1 + "activities/${id}/qualifications/stars",
        },
        reports: {
            types: v1 + "reports/types",
            store: {
                activity: v1 + "activities/${id}/reports",
                qualification: v1 + "qualifications/${id}/reports",
            },
        },
        subscriptions: {
            update: v1 + "users/me/activities/${id}/subscriptions",
            pay: v1 + "users/me/activities/${id}/subscriptions/pay",
        },
        users: {
            login: v1 + "users/login",
            socialAuth: v1 + "users/login/${socialNetwork}",
            loginWith: v1 + "users/login/${socialNetwork}?code=${code}",
            me: v1 + "users/me",
            logout: v1 + "users/me/login",
            register: v1 + "users/register",
            resend: v1 + "users/verification",
            reset: v1 + "users/reset/password",
            resetPassword: v1 + "users/reset/password/${code}",
            verification: v1 + "users/verification/${code}",
            deleteAccount: v1 + "users/me",
            password: v1 + "users/me/password",
        },
        visits: {
            statistics: {
                total: v1 + "users/me/activities/${id}/statistics/visits/total",
                perMonth: v1 + "users/me/activities/${id}/statistics/visits/${year}/per-month",
            },
        },
        admin: {
            users: {
                get: v1 + "admins/me/users?page=${page}",
                create: v1 + "admins/me/users",
                delete: v1 + "admins/me/users",
                roles: v1 + "admins/me/users/roles",
            },
            reports: {
                get: v1 + "admins/me/reports?page=${page}",
                actions: v1 + "admins/me/reports/actions?entity=${entity}",
                process: v1 + "admins/me/reports/${id}",
            },
        },
    },
};