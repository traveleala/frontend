import SecureLS from "secure-ls";
const ls = new SecureLS({ encodingType: 'aes', isCompression: true });

const storage = {
    set: (key, value) => {
        ls.set(key, value);

        return ls.get(value);
    },
    get: (key) => ls.get(key),
    remove: (key) => ls.remove(key)
}

export default storage;