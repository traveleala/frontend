import Config from "../config";

export default () => {
    if (!("geolocation" in navigator)) {
        this.errorStr = "Geolocation is not available.";
        return;
    }

    if (!localStorage.getItem("latitude") && !localStorage.getItem("longitude")) {
        navigator.geolocation.getCurrentPosition(
            (pos) => {
                localStorage.longitude = pos.coords.longitude;
                localStorage.latitude = pos.coords.latitude;
                localStorage.zoom = Config.map.zoom;
            },
            () => {
                localStorage.longitude = Config.map.defaultLongitude;
                localStorage.latitude = Config.map.defaultLatitude;
                localStorage.zoom = Config.map.zoom;
            }
        );
    }
};