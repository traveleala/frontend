import title from "../helpers/title";

export default {
    title,
    install(Vue) {
        Vue.prototype.$title = title;
    },
};