const errorHandler = (error) => {
    const response = error.response;
    let errorsObject = {};

    if (response && response.status == 400) {
        const errors = response.data.errors;

        for (error in errors) {
            errorsObject[error] = errors[error];
        }
    }

    return errorsObject;
};

export default {
    errorHandler,
    install(Vue) {
        Vue.prototype.$errorHandler = errorHandler;
    },
};