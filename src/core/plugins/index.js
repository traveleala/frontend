import Vue from "vue";
import Config from "../config";
import Vue2Filters from "vue2-filters";
import {
    BModal,
    BDropdown,
    BDropdownItem,
    BDropdownText,
    BTable,
    BInputGroup,
    BFormInput,
    BFormTextarea,
    BFormSelectOption,
    BFormSelect,
    BSidebar,
    VBTooltipPlugin,
    ProgressPlugin,
    BButton,
    BDropdownForm,
    BCollapse
} from "bootstrap-vue";

var SocialSharing = require("vue-social-sharing");
import {
    VLazyImagePlugin
} from "v-lazy-image";
import PrettyCheckbox from "pretty-checkbox-vue";
import VueTimeago from "vue-timeago";
import VueLayers from "vuelayers";
import {
    Overlay
} from "vuelayers";
import VueTypedJs from "vue-typed-js";
import VueClipboard from "vue-clipboard2";
import VeeValidate, {
    Validator
} from "vee-validate";
import es from "vee-validate/dist/locale/es";
import storage from "./storage";
import errorHandler from "./errorHandler";
import route from "./route";
import title from "./title";
import notification from "./notification";
import formData from "./formData";
import {
    VueReCaptcha
} from "vue-recaptcha-v3";
import resizeImage from "./resizeImage";

VueClipboard.config.autoSetContainer = true;

// Bootstrap vue
Vue.component("b-modal", BModal);
Vue.component("b-dropdown", BDropdown);
Vue.component("b-dropdown-item", BDropdownItem);
Vue.component("b-dropdown-text", BDropdownText);
Vue.component("b-table", BTable);
Vue.component("b-form-group", BInputGroup);
Vue.component("b-form-input", BFormInput);
Vue.component("b-form-textarea", BFormTextarea);
Vue.component("b-form-select", BFormSelect);
Vue.component("b-form-select-option", BFormSelectOption);
Vue.component("b-sidebar", BSidebar);
Vue.component("b-button", BButton);
Vue.component("b-dropdown-form", BDropdownForm);
Vue.component('b-collapse', BCollapse)
Vue.use(VBTooltipPlugin);
Vue.use(ProgressPlugin);

// Plugins
Vue.use(resizeImage);
Vue.use(formData);
Vue.use(route);
Vue.use(notification);
Vue.use(title);
Vue.use(errorHandler);
Vue.use(storage);
Vue.use(VueTypedJs);
Vue.use(SocialSharing);
Vue.use(Vue2Filters);
Vue.use(VLazyImagePlugin);
Vue.use(PrettyCheckbox);
Vue.use(Overlay);
Vue.use(VueLayers);
Vue.use(VueClipboard);
Vue.use(VueTimeago, {
    name: "Timeago",
    locale: "es",
    locales: {
        es: require("date-fns/locale/es"),
    },
});
Vue.use(VeeValidate, {
    errorBagName: "vErrors",
    events: "change|blur",
});
Validator.localize("es", es);
Vue.use(VueReCaptcha, {
    siteKey: Config.captchaKey,
    loaderOptions: {
        useRecaptchaNet: true,
        autoHideBadge: true,
    },
});