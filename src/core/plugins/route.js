import routeHelper from "../helpers/route";

export default {
    routeHelper,
    install(Vue) {
        Vue.prototype.$routeHelper = routeHelper;
    },
};