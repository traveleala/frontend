import formData from "../helpers/formData";

export default {
    formData,
    install(Vue) {
        Vue.prototype.$formData = formData;
    },
};