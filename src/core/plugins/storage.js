import storage from "../helpers/storage";

export default {
    storage,
    install(Vue) {
        Vue.prototype.$storage = storage;
    },
};