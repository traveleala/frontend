import resizeImage from "../helpers/resizeImage";

export default {
    resizeImage,
    install(Vue) {
        Vue.prototype.$resizeImage = resizeImage;
    },
};