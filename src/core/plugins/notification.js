import notification from "../helpers/notification";

export default {
    notification,
    install(Vue) {
        Vue.prototype.$notification = notification;
    },
};