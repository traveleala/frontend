import Vue from "vue";
import App from "~m/app/App";
import router from "./core/router";
import styles from "./core/styles"; // eslint-disable-line no-unused-vars
import plugins from "./core/plugins"; // eslint-disable-line no-unused-vars
import mixins from "./core/mixins"; // eslint-disable-line no-unused-vars
import directives from "./core/directives"; // eslint-disable-line no-unused-vars
import config from "./core/config";
import store from "./core/store";
import VueI18n from "vue-i18n";
import * as Sentry from "@sentry/browser";
import { Vue as VueIntegration } from "@sentry/integrations";
import VueGtag from "vue-gtag";

Sentry.init({
    dsn: config.sentry,
    integrations: [
        new VueIntegration({
            Vue,
            attachProps: true,
            logErrors: true,
        }),
    ],
});

Vue.use(
    VueGtag, {
        config: { id: config.googleAnalytics },
        pageTrackerScreenviewEnabled: true,
        appName: config.appName,
    },
    router
);

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.performance = true;

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: "es",
});

new Vue({
    i18n,
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");

window.addEventListener("message", (e) => {
    if (
        e.data &&
        typeof e.data === "string" &&
        e.data.match(/webpackHotUpdate/)
    ) {
        console.log("hot reload happened");
        console.clear();
    }
});