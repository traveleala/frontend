var merge = require("webpack-merge");
var path = require("path");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");
const webpack = require("webpack");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MD5 = require("crypto-js/md5");

module.exports = {
    devServer: {
        host: "localhost",
        port: 8080,
        disableHostCheck: true,
        https: true,
        open: false,
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: '@import "@/core/styles/sass/theme/_all.scss";',
                sassOptions: {
                    includePaths: ["./node_modules"],
                },
            },
        },
    },

    // configureWebpack: {
    //     optimization: {
    //         runtimeChunk: "single",
    //         splitChunks: {
    //             chunks: "all",
    //             maxInitialRequests: Infinity,
    //             minSize: 0,
    //             cacheGroups: {
    //                 vendor: {
    //                     test: /[\\/]node_modules[\\/]/,
    //                     name(module) {
    //                         const packageName = module.context.match(
    //                             /[\\/]node_modules[\\/](.*?)([\\/]|$)/
    //                         )[1];
    //                         return `npm.${packageName.replace("@", "")}`;
    //                     },
    //                 },
    //             },
    //         },
    //     },
    // },
    configureWebpack: {
        optimization: {
            runtimeChunk: "single",
            minimizer: [
                new OptimizeCSSAssetsPlugin({
                    cssProcessorPluginOptions: {
                        preset: ["default", { discardComments: { removeAll: true } }],
                    },
                }),
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    uglifyOptions: {
                        compress: false,
                        ecma: 6,
                        mangle: true,
                    },
                    sourceMap: true,
                }),
            ],
            splitChunks: {
                chunks: "all",
                maxInitialRequests: Infinity,
                minSize: 0,
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            let packageName = module.context.match(
                                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
                            )[1];
                            packageName = MD5(packageName.replace("@", "")).toString();
                            return packageName;
                        },
                    },
                    styles: {
                        test: /\.css$/,
                        name: "styles",
                        chunks: "all",
                        enforce: true,
                    },
                },
            },
        },
        plugins: [
            new webpack.EnvironmentPlugin("production"),
            new MiniCSSExtractPlugin({
                filename: "css/[name].[hash].css",
                chunkFilename: "css/[id].[hash].css",
            }),
            new CompressionPlugin({
                filename: "[path].gz[query]",
                algorithm: "gzip",
                test: new RegExp("\\.(js|css)$"),
                threshold: 10240,
                minRatio: 0.8,
            }),
            new webpack.HashedModuleIdsPlugin(),
        ],
    },
    chainWebpack: (config) => {
        config.module
            .rule("vue")
            .use("vue-loader")
            .tap((options) =>
                merge(options, {
                    transformAssetUrls: {
                        video: ["src", "poster"],
                        source: "src",
                        img: "src",
                        image: "xlink:href",
                        "vl-style-icon": "src",
                    },
                })
            );

        config.externals({
            moment: "moment",
        });

        config.resolve.alias
            .set("~c", path.resolve(__dirname, "src/core"))
            .set("~m", path.resolve(__dirname, "src/modules"));

        config.module
            .rule("i18n")
            .resourceQuery(/blockType=i18n/)
            .type("javascript/auto")
            .use("i18n")
            .loader("@kazupon/vue-i18n-loader")
            .end();

        config
            .plugin("webpack-bundle-analyzer")
            .use(BundleAnalyzerPlugin)
            .init((Plugin) => new Plugin({}));

        config.plugins.delete("prefetch");
        config.plugin("CompressionPlugin").use(CompressionPlugin);
    },
    runtimeCompiler: true,
};